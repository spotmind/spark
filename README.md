Este README proporciona una guía paso a paso para crear un proyecto utilizando SCSS (Sass), un preprocesador CSS que mejora la eficiencia y la organización de estilos en tus proyectos web.

Requisitos previos
Node.js y npm deben estar instalados en tu sistema. Si aún no los tienes, descárgalos e instálalos desde el sitio oficial de Node.js: https://nodejs.org/
Pasos para configurar el proyecto SCSS
Paso 1: Inicializar el proyecto
Crea una nueva carpeta para tu proyecto y ábrela en tu editor de código favorito. Luego, abre una terminal en esa carpeta y ejecuta el siguiente comando para inicializar un nuevo proyecto de npm:

bash
```
npm init -y
```
Esto creará un archivo package.json con la configuración predeterminada.

Paso 2: Instalar Sass (SCSS)
Para utilizar Sass en tu proyecto, necesitas instalar el paquete node-sass. Ejecuta el siguiente comando en tu terminal:

bash
```
npm install node-sass --save-dev
```
Esto instalará node-sass como una dependencia de desarrollo en tu proyecto y se agregará al archivo package.json.




Paso 2.1: Instalar browser-sync
Para agregar el script que haga el reload automático de los cambios después de ejecutar npm run watch-css, puedes utilizar una herramienta como browser-sync. Esta herramienta te permite crear un servidor local que recargará automáticamente tu página web cada vez que detecte cambios en los archivos CSS.
Ejecuta el siguiente comando en tu terminal para instalar browser-sync como una dependencia de desarrollo en tu proyecto:

bash
```
npm install browser-sync --save-dev
```

Paso 3: Configurar la estructura de carpetas
Crea una estructura básica de carpetas para tu proyecto. Una sugerencia podría ser:

css
```
- tu-proyecto/
  |- src/
  |  |- styles/
  |     |- main.scss
  |- public/
  |  |- css/
  |     |- main.css
  |- index.html

```  
En la carpeta src, colocaremos nuestros archivos SCSS, y en la carpeta dist, se generará automáticamente el archivo CSS compilado a partir de los archivos SCSS.

Paso 4: Crear el archivo main.scss
Dentro de la carpeta src/styles, crea un archivo llamado main.scss. Este será el punto de entrada donde importaremos otros archivos SCSS y donde comenzaremos a escribir nuestro código SCSS.

Por ejemplo, en main.scss, podrías tener algo como esto:

scss
```
/* Importar otros archivos SCSS aquí */
/* Ejemplo: */
@import 'variables';
@import 'botones';
@import 'estilos-generales';

/* Estilos específicos para esta página */
body {
  font-family: Arial, sans-serif;
  color: #333;
}
```
Paso 5: Compilar SCSS a CSS
Ahora, necesitamos configurar un comando para compilar nuestros archivos SCSS en CSS. Agrega los siguientes scripts en el archivo package.json:

json
```
"scripts": {
  "build-css": "node-sass src/styles/main.scss public/css/main.css",
  "watch-css": "node-sass src/styles/main.scss public/css/main.css --watch & browser-sync start --server 'public' --files 'public/css/main.css' 'public/index.html'"

}
```
Esto crea dos scripts: build-css, que compilará los archivos SCSS una sola vez, y watch-css, que observará los cambios en los archivos SCSS y los compilará automáticamente cada vez que se realice un cambio.

Paso 6: Compilar y ver cambios en tiempo real
Para compilar los archivos SCSS en CSS, ejecuta el siguiente comando en tu terminal:

bash
```
npm run build-css
```
Este comando compilará el archivo main.scss ubicado en src/styles y generará un archivo main.css en la carpeta dist.

Para trabajar en tiempo real y que los cambios en los archivos SCSS se reflejen automáticamente en el archivo CSS compilado, ejecuta el siguiente comando en tu terminal:

bash
```
npm run watch-css
```
¡Listo! Ahora tienes un proyecto configurado con SCSS para trabajar de manera más eficiente y organizada en tus estilos. Puedes comenzar a escribir tu código SCSS en main.scss, importar otros archivos SCSS y organizar tu proyecto como desees. A medida que guardas los cambios, verás que el archivo CSS en dist se actualiza automáticamente.

Recuerda que este es solo un inicio básico para trabajar con SCSS. Puedes expandir y ajustar la estructura de carpetas y configuraciones según las necesidades específicas de tu proyecto y preferencias personales. ¡Diviértete y disfruta creando estilos más poderosos con SCSS!

Publicar en GitLab Pages - Guía de Desarrollador
Si deseas publicar tu proyecto en GitLab Pages, sigue estos pasos sencillos:

1. Configurar el archivo .gitlab-ci.yml
Asegúrate de tener un archivo .gitlab-ci.yml en la raíz de tu repositorio con la siguiente estructura:

yaml
```
image: alpine:latest

pages:
  stage: deploy
  script:
    - echo 'Nothing to do...'
  artifacts:
    paths:
      - public
    expire_in: 1 day
  only:
    - main
    ```
Este archivo define la configuración necesaria para la implementación (deploy) de tu proyecto en GitLab Pages. En este ejemplo, estamos usando una imagen de Docker llamada alpine:latest como base para la ejecución. La etapa pages se encargará de desplegar los archivos que se encuentren en la carpeta public. El bloque only asegura que el pipeline se ejecute solo cuando se hagan cambios en la rama main.

2. Crear la carpeta public
Asegúrate de tener una carpeta llamada public en la raíz de tu proyecto y coloca todos los archivos que deseas que sean desplegados en GitLab Pages dentro de esta carpeta. Por ejemplo, si tienes un sitio web estático, aquí estarán los archivos HTML, CSS, imágenes, etc.

3. Ejecutar el pipeline
Una vez que hayas realizado los pasos anteriores y hayas confirmado los cambios en el repositorio, GitLab iniciará automáticamente el pipeline definido en el archivo .gitlab-ci.yml. El pipeline ejecutará las tareas necesarias para desplegar tu proyecto en GitLab Pages.

4. Consultar la URL de despliegue
Una vez que el pipeline haya sido ejecutado con éxito, podrás acceder a tu sitio web desplegado en GitLab Pages. La URL será similar a esta: https://<username>.gitlab.io/<project-name>. En tu caso particular, la URL sería: https://spotmind.gitlab.io/spark/.

¡Listo! Ahora tu proyecto está disponible en GitLab Pages para que cualquiera pueda verlo en línea.

Recuerda que cada vez que realices cambios en tu proyecto y los confirmes en la rama main, el pipeline de GitLab CI/CD se ejecutará automáticamente y actualizará tu sitio web en GitLab Pages.